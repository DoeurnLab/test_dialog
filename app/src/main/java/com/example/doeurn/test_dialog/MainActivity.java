package com.example.doeurn.test_dialog;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity
implements List_Dialog.Callback{
    Button btn1, btn2;
    TextView tvDisplay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn1 = (Button) findViewById(R.id.btn_list);
        btn2 = (Button) findViewById(R.id.mul);

        tvDisplay= (TextView) findViewById(R.id.tv_display);

        ;
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                final String[] color = new String[]{
                  "Red","Yellow", "Black"

                };
                boolean[] checkColor = {
                    false, false, false
                };
                builder.setMultiChoiceItems(color, null, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i, boolean b) {
                        if(b)
                        Toast.makeText(MainActivity.this,color[i], Toast.LENGTH_SHORT).show();
                    }
                });
                builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        Toast.makeText(MainActivity.this, "ok", Toast.LENGTH_SHORT).show();
                    }
                });

                builder.show();



            }
        });

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List_Dialog list_dialog = new List_Dialog();
                list_dialog.show(getFragmentManager(),"List Item");
            }
        });
    }

    @Override
    public void getData(String s) {
        Log.e("data",s);
        tvDisplay.setText(s);
    }
}
