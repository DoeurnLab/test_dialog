package com.example.doeurn.test_dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

/**
 * Created by Doeurn on 11/8/17.
 */

public class List_Dialog extends DialogFragment {
    //String list_item[] = {"cambodia","Thai","Korea"};
    String[] list_item;

    Callback callBack;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        list_item=context.getResources().getStringArray(R.array.list_item);
        callBack= (Callback) context;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder list_dialog = new AlertDialog.Builder(getActivity());
        list_dialog.setTitle("List Dialog");
       list_dialog.setIcon(R.drawable.ic_insert_emoticon_black_24dp);
        list_dialog.setItems(list_item, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                callBack.getData(list_item[i]);
                Toast.makeText(getActivity(), list_item[i], Toast.LENGTH_SHORT).show();
            }
        });
        return list_dialog.create();
    }

    interface Callback{
        void getData(String s);
    }
}
